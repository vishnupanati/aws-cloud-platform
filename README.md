- [Automation Logic Cloud Platform](#automation-logic-cloud-platform)
  - [How to run](#how-to-run)
    - [Prerequisites](#prerequisites)
    - [Quick Start Steps](#quick-start-steps)
    - [Parameters Conditions Explained](docs/CONFIGURATION.md)
  - [Description of Operation](docs/OPERATION.md)
  - [Description of files](docs/MANIFEST.md)
  - [Advanced Operations](docs/ADVANCED.md)
  - [Contributing](CONTRIBUTING.md)

# Automation Logic Cloud Platform

This project is a modular cloud platform framework which is built on top of AWS services to provide a single-click, out of the box solution for running applications on the cloud in a secure, consistent and easy to manage manner. This cloud platform has been built based on Automation Logic's experience of creating and managing 55+ bespoke cloud platforms for our various clients.

The objective of this project is to provide a one-click solution for organizations and developers who need to focus on the application and worry less about creating and managing the infrastructure of the platform, while simultaneously providing flexibility and customizability for more experienced dev-ops users.

The ALCP (Automation Logic Cloud Platform) implements best practices in cloud automation and agile software development and is meant to provide a stable and yet easily customizable solution for organizations seeking a quick starting point to deploy their applications to the cloud.

The ALCP currently supports AWS as a cloud provider with an Azure specific version currently under development.

This repository contains several simple CRUD applications written in a variety of different languages and all their associated configurations. These sample application stacks can be deployed from the AWS service catalog once the master template has been provisioned.

Each application stack will create a templated infrastructure with autoscaling instances or containers, load balancing, security, logging, metrics, monitoring and a continuous integration / continuous deployment (CI/CD) pipeline. Once the application code is pushed to the repository, the pipeline should build and deploy the web application automatically.

This project is released under the [Apache License](https://www.apache.org/licenses/LICENSE-2.0).

## How to run

To be able to deploy this you need an AWS account.
You can use the [AWS User Interface](https://console.aws.amazon.com/console/home) to deploy the code as well as the [CLI](https://aws.amazon.com/cli/).

### Prerequisites

There are 9 regions supporting all the resources required to create the stack:

 * Virginia: **us-east-1**
 * Ohio: **us-east-2**
 * Oregon: **us-west-2**
 * Montreal: **ca-central-1**
 * Ireland: **eu-west-1**
 * London: **eu-west-2**
 * Frankfurt: **eu-central-1**
 * Sydney: **ap-southeast-2**
 * Tokyo: **ap-northeast-1**

Unsupported Regions and Missing Resources on AWS
* Northern California - Service Catalog 
* Sao Paulo - Glacier, Service Catalog, Code Build
* Singapore - Glacier
* Seoul - Service Catalog, Code Build
* Mumbai  - Service Catalog, Code Build

If you intend on using the [AWS CLI](https://aws.amazon.com/cli/) you must configure the following:

 * AWS IAM user within product development account
 * AWS CLI installed
 * AWS credentials configured

If you intend to use git to push some code to AWS CodeCommit, please follow the instructions on this [page](http://docs.aws.amazon.com/codecommit/latest/userguide/setting-up-ssh-unixes.html?icmpid=docs_acc_console_connect)

### Quick Start Steps

The following steps need to be run in order to get the demo to work;

1.  Launch the mastertemplate.yaml template.  
    This can be done by one of the following methods:
      - Loading mastertemplate.yaml in the [AWS Cloudformation Interface](https://eu-west-1.console.aws.amazon.com/cloudformation/) in one of the above regions, with the default parameters.
      
           Click the following button to do this automatically (launches in the eu-west-1 'Ireland' region)  
      [![Launch Stack](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home#/stacks/new?region=eu-west-1&stackName=service-stack&templateURL=https://s3.amazonaws.com/al-cf-templates-us-east-1/templates/mastertemplate.yaml)

      - Using the AWS CLI to create a stack.  
        ```
        aws cloudformation deploy --region "eu-west-1" --template-file "mastertemplate.yaml" --stack-name "service-stack" --capabilities "CAPABILITY_NAMED_IAM" --parameter-overrides Cleanup="Enable"
        ```


2. The stack creates an [AWS CodeCommit](https://aws.amazon.com/codecommit/) repository as part of the infrastructure, you must push the files from this repository to it. This will run the CI/CD pipeline for the master stack and provision the service catalog with the demo products.

      - First you need to get your git repo URL (from CodeCommit) to push your code. You can go to AWS CodeCommit UI or use the AWS CLI:

        ```
        GIT_REMOTE=$(aws cloudformation describe-stacks --region "eu-west-1" --stack-name "service-stack"  --query "Stacks[].Outputs[?OutputKey=='RepoURL'].OutputValue"  --output text)
        ```

      - Then add a new remote repository to your git and push the code

        ```
        git remote add codecommit ${GIT_REMOTE};     
        git push codecommit master
        ```

3. Open the [AWS Service Catalog web interface](https://console.aws.amazon.com/servicecatalog/home?#/dashboard) on the AWS Console and select a demo product and launch it.
    -  You will need to select your EC2 SSH Key in the KeyName parameter. This needs to be created from the [AWS EC2 web interface](https://console.aws.amazon.com/ec2/v2/home?#KeyPairs:sort=keyName) if you don't have one already.
    -  You will need to provide a database username & password which will be created during stack creation.
    -  Each demo listed as the products in the service catalog will launch a different application using the standardized infrastructure of the ALCP with some minor application specific changes.
    -  **Note:** If no products are listed after running the steps above, you may need to add yourself to the appropriate user group that the product is shared on. These details can be found in the *portfolio/mappings.yaml* file.    

4. The stack creates an AWS CodeCommit repository as part of the infrastructure, you must push the files from this repository to it. This will run the CI/CD pipeline for the product stack and provision the demo web application for that product in a few minutes.

      - First you need to get your git repo URL (from CodeCommit) to push your code. You can go to AWS CodeCommit UI or use the AWS CLI:

        ```
        GIT_REMOTE=$(aws cloudformation describe-stacks --region "eu-west-1" --stack-name "presentation"  --query "Stacks[].Outputs[?OutputKey=='RepoURL'].OutputValue"  --output text)
        ```

      - Then add a new remote repository to your git and push the code

        ```
        git remote add codecommit ${GIT_REMOTE};
        git push codecommit master
        ```

5. When the platform / stack is no longer needed, it can be terminated from the list of provisioned products in the service catalogue.
      - If the **auto cleanup** options are enabled, the retained resources of a stack will be cleaned up automatically.
      - If **manual cleanup** is required, the *utils/cleanup/cleanup.sh* script can be used. To use run the following command from the terminal.

        ```
        PREFIX=<resource prefix> REGION=<region> ./utils/cleanup/cleanup.sh
        ```

      - The **mastertemplate.yaml** needs to be deleted with the **manual cleanup** as auto cleanup is not possible. To use run the following command from the terminal.

        ```
        STACK_NAME=<Stack_Name> REGION=<region> MASTER=true ./utils/cleanup/cleanup.sh
        ```
