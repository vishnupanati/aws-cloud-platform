- [Description of Operation](#description-of-operation)
  - [Service Catalogue](#service-catalogue)
  - [Cloud Platform](#cloud-platform)
    - [CI / CD Pipeline](#ci-cd-pipeline)
    - [Secrets and Config Management](#secrets-and-config-management)
    - [Logging, Metrics and Monitoring](#logging-metrics-and-monitoring)
      - [Log Groups and Alarms](#log-groups-and-alarms)
      - [Log Retention](#log-retention)
      - [Elastic Search Logging](#elastic-search-logging)
      - [DASHBOARD](#dashboard)
        - [CLOUDWATCH](#cloudwatch)
        - [KIBANA](#kibana)

# Description of Operation

## Service Catalogue

[AWS Service Catalog](https://aws.amazon.com/servicecatalog/) allows organizations to create and manage catalogs of IT services that are approved for use on AWS. These IT services can include everything from virtual machine images, servers, software, and databases to complete multi-tier application architectures. AWS Service Catalog allows you to centrally manage commonly deployed IT services, and helps you achieve consistent governance and meet your compliance requirements, while enabling users to quickly deploy only the approved IT services they need. 

The project utilizes the AWS Service Catalog to create and manage a list of products which are created dynamically. There is a CI/CD pipeline setup which allows the template author to instantly deploy his changes to the service catalog and thereby allow users to provision the products.

The products that are being deployed from the service catalog are primarily the cloud platform with support for different application types. A different application type or database type can easily be assembled from the base infrastructure with minimal effort and and new application integrated.

![Scheme](img/service_catalog.png)

An administrator or template author can add stacks to the product portfolio by committing them to the source control repository where they are then automatically built and deployed to the service catalogue.

Developers and administrators are then able to select products to build from the service catalogue.  Roles and privileges can be used to constrain users to specific products, and to specific parameters when building products, such as resource limits and access to data.

## Cloud Platform

The Automation Logic Cloud Platform (ALCP) will automate the creation of a cloud platform architecture with modular sub-systems which can support any application code that needs to be run on the platform.
The objective of this project is to provide a one-click solution for organizations and developers who need to focus on the application and worry less about creating and managing the infrastructure of the platform, while simultaneously providing flexibility and customizability for more experienced dev-ops users.

The core of the platform is the networking infrastructure, the database and the auto-scaling web servers. This is complemented by three separate sub-systems namely

 * The CI / CD Pipeline Sub-System
 * The Secrets and Configuration Management Sub-System
 * The Logging, Metrics and Monitoring Sub-System

Each of the above sub-systems can be removed, modified, supplemented or replaced based on specific requirements. Users of the ALCP are provided the liberty to perform these alterations themselves or Automation Logic can perform the required changes on a time and materials basis.

There are several similar products or stacks available to choose from in this project. Each of these has minor variations to support different common application and database types. This also additionally demonstrates the flexibility of the ALCP, that adding support for other applications or databases should be trivial.

A sample reference architecture is outlined in the below diagram.

*Note: This architecture may be different based on the specific architecture chosen and based on other parameters chosen prior to deploying the product.*

![Scheme](img/cloud-platform.png)

### CI / CD Pipeline

The CI / CD pipeline sub-system is made up of four main components.

 * [AWS CodeCommit](https://aws.amazon.com/codecommit/) -> Is an easily replaceable GIT repository which can be swapped out for [GitHub](https://github.com/).  
 * [AWS CodeBuild](https://aws.amazon.com/codebuild/details/) -> Is an on-demand build server. Can be replaced with [Jenkins](https://jenkins.io/) or other tools.  
 * [AWS CodeDeploy](https://aws.amazon.com/codedeploy/) -> A deployment agent to deploy code to the Instances or Virtual Machines. Can be replaced with Jenkins or other tools.  
 * [AWS CodePipeline](https://aws.amazon.com/codepipeline/) -> A pipeline that synchronizes and triggers the other three components. Can be replaced with Jenkins or other tools.

This sub-system is modular and can be replaced entirely or have parts of it replaced by other 3rd party tools such as Jenkins.

The developers who use the platform can be given access to commit their code to the repository (CodeCommit or GitHub).

Once a new commit is detected in the *Master* branch the CodePipeline will notify the build server to pull the latest changes and initiate a build.

The build instructions are coded in a **buildspec.yml** file which is either a standalone file in the repository or is inline within the product template. 
This project uses inline buildspec files in the product template.

We use CodeBuild to not only build the artifact, we also use it to run **Unit Tests** and **Integration Tests**.

Once all tests have passed, the CodePipeline initiates the deployment using CodeDeploy. The CodeDeploy agent is an agent which is installed on the server during server launch, it is triggered to pull down the built artifact and executes the deployment instructions which are provided in an **appspec.yml** file which is present in the root directory of the artifact.

There is also a manual approval step which can be added to prevent continuous deployment while still performing continuous integration. An approver with sufficient privileges needs to approve each build before its deployed in this case. This project includes the manual approval step however it is at the very end of the pipeline after deployment and as such is for demo purposes only, and does not prevent continuous deployments.

*Note: [ECS](https://aws.amazon.com/ecs/) deployment is currently not supported by [AWS CodeDeploy](https://aws.amazon.com/codedeploy/). In this project we use [AWS Cloudformation](https://aws.amazon.com/cloudformation/) to deploy ECS / Docker containers.*

AWS CI / CD pipeline example is outlined in the below diagram.

![Scheme](img/codepipeline.png)

### Secrets and Config Management

The secrets and config management sub-system relies on [AWS Parameter Store](https://aws.amazon.com/ec2/systems-manager/parameter-store/) to store configuration values and secrets. The secrets are encrypted using an [AWS KMS](https://aws.amazon.com/kms/) key which can encrypt and decrypt the values. [AWS IAM](https://aws.amazon.com/iam/) is used to provide fine granularity access control over which users or services can encrypt and which users and services can decrypt the values in the Parameter Store.

[AWS Parameter Store](https://aws.amazon.com/ec2/systems-manager/parameter-store/) can store three types of values;

  - String -> A string value
  - StringList -> A collection of strings
  - SecureString -> A string which is encrypted with a [AWS KMS](https://aws.amazon.com/kms/) Key

Since [AWS Cloudformation](https://aws.amazon.com/cloudformation/) does not provide support for storing SecureStrings, we use a custom **parameter store [AWS Lambda](https://aws.amazon.com/lambda/) function** to store secret SecureStrings in the AWS Parameter Store. This lambda function has only the privileges to use the KMS key for encrypting a value and not decrypting it. This process is seamless and carried out automatically during stack provisioning and preserves the secrets effectively.

### Logging, Metrics and Monitoring

The logging sub-system uses [AWS CloudWatch](https://aws.amazon.com/cloudwatch/) to log everything within the cloud platform. All logs and metrics are available in near real-time frequency.
Additionally [AWS Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/) can be enabled to provide additional logging capabilities.

There are two logging modes, Basic and Advanced.

  - Basic: Only the build server and application logs are logged.
  - Advanced: This will create log groups to log all activity including lambda functions on the stack.

#### Log Groups and Alarms

**Metrics**, either derived by from the logs or sent directly from the instances are also available in the CloudWatch dashboard.
There are a number of automated **Alarms** that make use of metrics to send out notifications to the administrator.
Additionally some automated recovery actions can be directly triggered by the alarms without user intervention.

The product log groups and possible alarm actions are outlined in the image below.

![Scheme](img/log_groups_alarms.png)

#### Log Retention

There is a log retention policy in place that migrates all logs to [AWS S3](https://aws.amazon.com/s3/) and gradually migrates them to long term storage such as [AWS Glacier](https://aws.amazon.com/glacier/). This is especially useful in situations where logs need to be preserved for long periods of time to meet audit or compliance requirements.

The Log migration and retention policy is outlined in the image below.

![Scheme](img/log_retention.png)

#### Elastic Search Logging

There is an additional option of using [AWS Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/) in addition to the standard CloudWatch Logs.
This option when enabled by parameter will stream log data to AWS Elasticsearch where it can be queried and visualized with the [Kibana](https://www.elastic.co/products/kibana) dashboard plugin.

There are two methods of using Elasticsearch.

   - Central -> Which requires a central logging product to be provisioned, from the service catalog, beforehand, in the same region. This will create a central Elasticsearch cluster.
   - Integrated -> Each stack will create and manage its own Elasticsearch cluster.

*Note: AWS Elasticsearch is expensive. Consider leaving it disabled if not specifically required for your use-case.*

*Note: AWS Elasticsearch can be secured by restricting it to an organization's IP. However Kibana cannot be secured using [AWS IAM](https://aws.amazon.com/iam/) policies which will allow anyone within an organization to view the Kibana dashboard. This is a known problem with the AWS Managed Elasticsearch service and as such cannot be fixed within the scope of this project. If you need to restrict Kibana dashboard to specific users or groups within the organization, consider alternatives such as a [self managed Elasticsearch](https://www.elastic.co/) cluster instead of the [AWS Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/).*

**Retention** of Elasticsearch cluster is achieved by a snapshot process which stores the snapshot in an AWS S3 bucket which is gradually moved into AWS Glacier for long term retention.
This uses an innovative *automated manual snapshot* process. Automatic snapshot capability is available natively with AWS Elasticsearch. However to restore to a snapshot version, the process involves emailing AWS Support who will then restore to a snapshot on the user's behalf. This process is tedious and can often cause significant delays. 

There is a *manual snapshot* capability where an Elasticsearch snapshot can be triggered with a CLI or API command. Restoring from these *manual snapshots* is trivial as it can also be triggered using a CLI or API command.
In order to provide a more robust automated snapshot capability, this project implements a custom [AWS Lambda](https://aws.amazon.com/lambda/) function which sends an API call to create a manual snapshot of Elasticsearch. This custom snapshot lambda is triggered once per day, by default, using [AWS Cloudwatch Event](http://docs.aws.amazon.com/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) rules which is used as an AWS equivalent of a Cron Job.

The method of operation where the logs are collected in AWS Cloudwatch Logs before being moved to AWS Managed Elasticsearch is outlined below.
This process is completely automated and the logs are available in near real-time in both Cloudwatch and Elasticsearch.


![Scheme](img/log_elasticsearch.png)

An alternative method of streaming logs to Elasticsearch is to use [AWS Kinesis](https://aws.amazon.com/kinesis/) as a ready buffer to hold logs data for up-to the past seven days. When the logs need to be analyzed an Elasticsearch cluster can be spun up and the logs from the Kinesis stream can be ingested to provide querying capability. Once the requirement is fulfilled, the Elasticsearch cluster can be taken offline to save costs. This method will provide an method to use Elasticsearch on-demand instead of constantly being run.

This feature is not currently implemented in this project, however it can be easily added on request, based on previously shelved work.
The method of operation is outlined below.

![Scheme](img/log_elasticsearch_alternate.png)

#### DASHBOARD

##### CLOUDWATCH

All the CloudWatch logs are collated in one location and can be viewed by the automated dashboard.
The URL for the dashboard is present as a stack output.

An example dashboard can be found in the image below.

![Scheme](img/dashboard.png)

##### KIBANA

If Elasticsearch is used, the Kibana dashboard is automatically populated using a dashboard specification which is uploaded during the build pipeline.

An example Kibana dashboard can be found in the image below.

![Scheme](img/kibana.png)

